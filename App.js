import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Screen, NavigationBar, Title, Icon, TextInput, Button, ListView, Subtitle, Image, Spinner, GridRow, ImageBackground, Tile, Lightbox } from '@shoutem/ui';

const baseURL = 'https://pixabay.com/api/'
const API_KEY = '9410845-35e669e0d2c37a7c3939872aa'

const formatQuery = (q = '') => q.split(' ').filter(v => Boolean(v)).join('+')

const API_URL = (query, page = 1) => `${baseURL}?key=${API_KEY}&q=${formatQuery(query)}&page=${page}&per_page=${page === 1 ? 21 : 20}`

const formatHits = (hits = []) => hits.map(hit => ({large: hit.largeImageURL, small: hit.previewURL}))

export default class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      searchQuery : '',
      searchText : '',
      imageList: [],
      page: 1,
      loading: false,
      totalHits: null
    }
  }

  makeApiCall = (searchQuery, page = this.state.page) => {
    fetch(API_URL(searchQuery, page))
      .then(response => response.status === 200 ? response.json() : response)
      .then(data => this.setState({
        imageList: [...this.state.imageList, ...formatHits(data.hits)],
        totalHits: data.totalHits,
        loading: false
      }))
    }

  getTitle = (searchQuery) => <Title>{ searchQuery ?  searchQuery : 'GALLERY'}</Title>

  submitEdit = (searchText) => {
    this.setState({searchQuery: searchText, imageList: [], searchText: '', loading: true}, () => {
      this.makeApiCall(searchText)
    })
  }

  getCloseButton = () => (
    <Button styleName="clear" onPress={() => this.setState({searchQuery: '', searchText: '', imageList: []})}>
      <Icon name="close" />
    </Button>
  )

  renderRow(rowData, sectionId, index) {
    if (index === '0') {
      return (
        <Image
          styleName="large"
          source={{ uri: rowData[0].small }}
        />
      );
    }

    const cellViews = rowData.map((image, id) => {
      return (
        <Image 
          key={id}
          styleName="medium-wide"
          source={{ uri: image.small  }}
        />
      );
    });

    return (
      <GridRow columns={2}>
        {cellViews}
      </GridRow>
    );
  }

  getNextPage = () => {
    this.setState({page: this.state.page + 1, loading: true}, () => {
      this.makeApiCall(this.state.searchQuery)
    })
  }

  render() {
    let isFirstImage = true
    const groupedData = GridRow.groupByRows(this.state.imageList, 2, () => {
      if (isFirstImage) {
        isFirstImage = false;
        return 2;
      }
      return 1;
    });
  
    return (
      <Screen>
        <NavigationBar
          centerComponent={this.getTitle(this.state.searchQuery)}
          rightComponent={this.getCloseButton()}
          styleName="inline"
        />
        <TextInput
          onChangeText={searchText => this.setState({searchText})}
          onSubmitEditing={() => this.submitEdit(this.state.searchText)}
          value={this.state.searchText}
          returnKeyType={'search'}
          autoCorrect={false}
          placeholder={'Enter search term here'}
        />
        {
          this.state.imageList.length ? (
            <ListView
              data={groupedData}
              renderRow={this.renderRow.bind(this)}
              onLoadMore={this.state.totalHits > this.state.imageList.length ? this.getNextPage.bind(this) : null}
              loading={this.state.loading}
            />
          ) : (
            this.state.imageList.length === 0 && this.state.searchQuery ? (
              this.state.loading ? (
                <View style={styles.container} styleName="space-between h-center v-center">
                  <Spinner styleName="large"/>
                  <Text>{'\n'}</Text>
                  <Subtitle>Searching for term: "{this.state.searchQuery}"</Subtitle>
                </View>
              ) : (
                <View style={styles.container} styleName="space-between h-center v-center">
                  <Subtitle>No results for the term: "{this.state.searchQuery}"</Subtitle>
                </View>
              )
            ) : (
              <View style={styles.container} styleName="space-between h-center v-center">
                <Subtitle>Please enter a search term</Subtitle>
              </View>
            )
          )}
      </Screen>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50
  }
});
